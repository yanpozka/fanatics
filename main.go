package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	port := ":" + getEnvStr("PORT", "8080")

	mux := http.NewServeMux()

	mux.Handle("/cert/", loggerPanic(http.HandlerFunc(handleCert)))

	srv := &http.Server{
		Addr:    port,
		Handler: mux,

		ReadTimeout:       getEnvDuration("READ_TIMEOUT_SECONDS", 10) * time.Second,
		ReadHeaderTimeout: getEnvDuration("READHEADER_TIMEOUT_SECONDS", 5) * time.Second,
		WriteTimeout:      getEnvDuration("WRITE_SECONDS", 15) * time.Second,
	}

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt, os.Kill)

	go func() {
		log.Printf("Serving on %q ...", port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
		}
	}()

	// has to be > 10 seconds in order to wait for all /cert/{domain} calls
	timeout := getEnvDuration("SHUTDOWN_TIMEOUT_SECONDS", 12) * time.Second

	// blocks until we get a terminal OS signal
	osSignal := <-ch
	log.Printf("Got OS signal: '%v', shuting down the server with timeout: %v ", osSignal, timeout)

	srv.SetKeepAlivesEnabled(false)

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Failed to shutdown the server: %v", err)
	}
}

func getEnvStr(name, defaultVal string) string {
	envVal := os.Getenv(name)
	if envVal == "" {
		return defaultVal
	}
	return envVal
}

func getEnvDuration(name string, defaultVal int) time.Duration {
	envVal := os.Getenv(name)
	if envVal == "" {
		return time.Duration(defaultVal)
	}
	num, _ := strconv.Atoi(envVal)
	return time.Duration(num)
}
