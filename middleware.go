package main

import (
	"log"
	"net/http"
	"runtime/debug"
	"time"
)

func loggerPanic(next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			val := recover()
			if val == nil {
				return
			}
			log.Printf("Recovering from panic with message: '%v'", val)
			debug.PrintStack()
		}()

		start := time.Now()
		//
		next.ServeHTTP(w, r)

		delta := time.Since(start)
		logRequest(r, delta)
	})
}

func logRequest(r *http.Request, d time.Duration) {
	log.Printf("[%s]: %s | Time consumed: %v", r.Method, r.RequestURI, d)
}
