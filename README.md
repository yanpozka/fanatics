### How to run it:

```bash
# renew certificates each 30 seconds:
#
export CERT_RENEWAL_SECONDS=30

go run *.go
# or
go build && ./fanatics
# or
go install && fanatics
```

environment variable `CERT_RENEWAL_SECONDS` represents the number of seconds for certificates renewal

### Test /cert/{domain} endpoint:

```bash
curl -i localhost:8080/cert/yourdomain.com
```

After requesting any number of certificates we can see how stoping the server (for example with Ctrl+C) make it waits for the end of current requests

#### Test hashing package:

```bash
cd certs 
go test -v .
```

##### Improvements (only if the code grows more):
 - Move /cert handler to a new package with its own context
 - Move middlewares to a new package
 - More tests
