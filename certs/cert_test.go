package certs

import (
	"testing"
	"time"
)

func TestMakeCert(t *testing.T) {
	table := [][]string{
		// input, expected cert
		{"abc", `3a81oZNherrMQXNJriBBMRLm+k6JqX6iCp7u5ktV05ohkpkqJ0/BqDa6PCOj/uu9RU1EI2Q86A4qmslPpUyknw==`},
		{"123", `PJkJr+wlNU1VHa4hWQuybjjVPyFzuNPcPu5MBH56scHri4UQPjvnumE7MbtcnDYhTcnxSkL9ei/bhIVrylxEwg==`},
	}

	for _, testCase := range table {
		if result := MakeCert(testCase[0], time.Second); result != testCase[1] {
			t.Fatalf("Expected cert: %q doesn't match calculated cert: %q", testCase[1], result)
		}
	}
}
