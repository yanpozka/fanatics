package certs

import (
	"crypto/sha512"
	"encoding/base64"
	"log"
	"sync"
	"time"
)

var mCache = new(sync.Map)

// HashBase64 returns a Base64 encoded string of a certificate
func MakeCert(domainName string, d time.Duration) string {
	cert, found := mCache.Load(domainName)
	if found {
		log.Printf("Certificate for domain name: %q was found in cache", domainName)
		return cert.(string)
	}

	cert = hash(domainName)
	// store new certificate on cache
	mCache.Store(domainName, cert)

	// after creating a new certificate start a new goroutine to renew the certificate with Duration d
	go func() {
		ticker := time.NewTicker(d)
		for {
			select {
			case <-ticker.C:
				log.Printf("Renewing certificate with domain name: %q", domainName)

				mCache.Store(domainName, hash(domainName))
			}
		}
	}()

	return cert.(string)
}

func hash(domainName string) string {

	hash512 := sha512.Sum512([]byte(domainName))

	return base64.StdEncoding.EncodeToString(hash512[:])
}
