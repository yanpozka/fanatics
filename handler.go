package main

import (
	"log"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/yanpozka/fanatics/certs"
)

const defaultRenewalSeconds = 11 // 1 hour in seconds

func handleCert(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	parts := strings.Split(r.RequestURI, "/")
	domainName := parts[len(parts)-1]
	if domainName == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	time.Sleep(10 * time.Second)

	d := getEnvDuration("CERT_RENEWAL_SECONDS", defaultRenewalSeconds) * time.Second
	cert := certs.MakeCert(domainName, d)

	w.WriteHeader(http.StatusOK)

	if _, err := w.Write([]byte("foo-" + cert)); err != nil {
		log.Printf("Error writing response: %q", err)
	}
}
